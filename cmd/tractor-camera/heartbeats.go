package main

import (
	"log"
)

func updateHeartbeats(heartbeats <-chan []byte) {
	for {
		heartbeat, ok := <-heartbeats
		if !ok {
			return
		}
		log.Print(string(heartbeat))
	}
}
