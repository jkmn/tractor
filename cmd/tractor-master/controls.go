package main

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/jkmn/errors"
	"google.golang.org/protobuf/proto"

	"gitlab.com/jkmn/tractor/pkg/gamepad"
	"gitlab.com/jkmn/tractor/pkg/relay"
)

func processInput(ctx context.Context, inputChan <-chan []byte, relayChan chan<- []byte) {
	log.Print("listening for input")

	var prevBytes string

	for {
		select {
		case <-ctx.Done():
			return
		case inputBytes := <-inputChan:
			relayBytes, err := mapToRelay(inputBytes)
			if err != nil {
				log.Print(errors.Stack(err))
				continue
			}
			_relayBytes := fmt.Sprintf("%x", relayBytes)
			if prevBytes != _relayBytes {
				relayChan <- relayBytes
				prevBytes = _relayBytes
			}
		}
	}
}

func mapToRelay(inputBytes []byte) ([]byte, error) {
	log.Print("processing input")
	gp := new(gamepad.Gamepad)

	err := proto.Unmarshal(inputBytes, gp)
	if err != nil {
		return nil, errors.Stack(err)
	}

	log.Print(gp.String())

	/*
		The arrangement of the relays is arbitrary at this point.

		R0: Swing Left
		R1: Swing Right
		R2: Stick Boom (Dipper) away
		R3: Stick Boom (Dipper) close
		R4: Bucket curl in (closed)
		R5: Bucket curl out (dump)
		R6: Main Boom down
		R7: Main Boom up
	*/

	// TODO: allow SAE controls
	relayBytes := mapToRelayISO(gp)

	// Eventually, we'll need to make the relay service accept multiple bytes.
	// We need to add support for drive, steering, and accessories.
	log.Printf("%08b", relayBytes[0])

	return relayBytes, nil
}

func mapToRelayISO(gp *gamepad.Gamepad) []byte {
	/*
		== ISO CONTROLS ==

		R0: Left hand left = Swing left.
		R1: Left hand right = Swing right.
		R2: Right hand forward = Stick Boom (Dipper) away.
		R3: Right hand back = Stick Boom (Dipper) close.
		R4: Right hand left = Bucket curl in (closed)
		R5: Right hand right = Bucket curl out (dump)
		R6: Left hand forward = Main Boom down.
		R7: Left hand back = Main Boom up.
	*/

	r := relay.NewRelay()
	r.Set(0, gp.StickLeft.GetX() < -10000)
	r.Set(1, gp.StickLeft.GetX() > 10000)
	r.Set(2, gp.StickRight.GetY() < -10000)
	r.Set(3, gp.StickRight.GetY() > 10000)
	r.Set(4, gp.StickRight.GetX() < -10000)
	r.Set(5, gp.StickRight.GetX() > 10000)
	r.Set(6, gp.StickLeft.GetY() < -10000)
	r.Set(7, gp.StickLeft.GetY() > 10000)

	return []byte{r.Byte()}
}

/*


== SAE CONTROLS ==

Left hand left = Swing left.
Left hand right = Swing right.
Left hand forward = Stick Boom (Dipper) away.
Left hand back = Stick Boom (Dipper) close.
Right hand left = Bucket curl in (closed)
Right hand right = Bucket curl out (dump)
Right hand forward = Main Boom down.
Right hand back = Main Boom up.
*/

func mapToRelaySAE(gp *gamepad.Gamepad) byte {
	r := new(relay.Relay)
	r.Set(0, gp.StickLeft.GetX() > 10000)
	r.Set(1, gp.StickLeft.GetX() < -10000)
	r.Set(2, gp.StickLeft.GetY() > 10000)
	r.Set(3, gp.StickLeft.GetY() < -10000)
	r.Set(4, gp.StickRight.GetX() > 10000)
	r.Set(5, gp.StickRight.GetX() > 10000)
	r.Set(6, gp.StickRight.GetY() > 10000)
	r.Set(7, gp.StickRight.GetY() > 10000)

	return r.Byte()
}
