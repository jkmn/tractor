package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/jkmn/errors"

	"gitlab.com/jkmn/tractor/pkg/common"
	"gitlab.com/jkmn/tractor/pkg/udp"
)

const INSTANCE_NAME string = common.InstanceNameMaster

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	err := sendAllHeartbeats(ctx)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	heartbeatChan, err := udp.ReceiveHeartbeats(ctx, INSTANCE_NAME, common.PortHeartbeatMaster)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	go updateHeartbeats(heartbeatChan)

	inputChan, err := udp.NewUDPReceiver(ctx, INSTANCE_NAME, common.ServiceNameInputSink, common.PortInputSink)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	relayChan, err := udp.NewUDPSender(ctx, common.InstanceNameRelay, common.ServiceNameRelaySink)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	go processInput(ctx, inputChan, relayChan)

	// Clean exit.
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	select {
	case <-sig:
		// Exit by user
		log.Print("shutting down")
		cancel()
		time.Sleep(1 * time.Second)
	}

}
