package main

import (
	"context"
	"log"

	"gitlab.com/jkmn/errors"

	"gitlab.com/jkmn/tractor/pkg/common"
	"gitlab.com/jkmn/tractor/pkg/udp"
)

func updateHeartbeats(heartbeats <-chan []byte) {
	for {
		heartbeat, ok := <-heartbeats
		if !ok {
			return
		}
		log.Print(string(heartbeat))
	}
}

func sendAllHeartbeats(ctx context.Context) error {

	ctx, cancel := context.WithCancel(ctx)

	instances := []string{
		common.InstanceNameInput,
		common.InstanceNameRelay,
		common.InstanceNameEngine,
	}

	for _, instance := range instances {
		err := udp.SendHeartbeats(ctx, INSTANCE_NAME, instance)
		if err != nil {
			cancel()
			return errors.Stack(err)
		}
	}

	return nil
}
