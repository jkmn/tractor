package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/jkmn/errors"

	"gitlab.com/jkmn/tractor/pkg/common"
	"gitlab.com/jkmn/tractor/pkg/serial"
	"gitlab.com/jkmn/tractor/pkg/udp"
)

const INSTANCE_NAME string = common.InstanceNameRelay

var (
	useArduino = flag.Bool("arduino", false, "use USB-connected Arduinos for GPIO and I2C")
)

var arduinoPath string

func main() {

	flag.Parse()

	var err error

	if *useArduino {
		arduinoPath, err = serial.FindSerial("RELAY0")
		if err != nil {
			log.Fatal(errors.Stack(err))
		}

		if arduinoPath == "" {
			log.Fatal("RELAY0 not found.")
		}
	}

	ctx, cancel := context.WithCancel(context.Background())

	heartbeatChan, err := udp.ReceiveHeartbeats(ctx, INSTANCE_NAME, common.PortHeartbeatRelay)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	go updateHeartbeats(heartbeatChan)

	err = udp.SendHeartbeats(ctx, INSTANCE_NAME, common.InstanceNameMaster)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	relayChan, err := udp.NewUDPReceiver(ctx, INSTANCE_NAME, common.ServiceNameRelaySink, common.PortRelaySink)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	go processInput(ctx, relayChan)

	// Clean exit.
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	select {
	case <-sig:
		// Exit by user
		log.Print("shutting down")
		cancel()
		time.Sleep(1 * time.Second)
	}
}

func processInput(ctx context.Context, ch <-chan []byte) {

	log.Print("listening for input")

	for {
		select {
		case <-ctx.Done():
			return
		case b := <-ch:
			var err error
			log.Printf("%08b", b[0])
			if *useArduino {
				err = updateRelaysArduino(b)
			} else {
				err = updateRelays(b)
			}
			if err != nil {
				log.Print(errors.Stack(err))
			}
		}
	}
}

func updateRelays(b []byte) error {
	if len(b) != 0 {
		return nil
	}

	// TODO: connect relays via GPIO.
	log.Printf("%08b", b[0])

	return nil
}

func updateRelaysArduino(b []byte) error {

	if len(b) == 0 {
		return nil
	}

	err := serial.Write(arduinoPath, []byte{b[0], '\n'})
	if err != nil {
		return errors.Stack(err)
	}
	return nil
}
