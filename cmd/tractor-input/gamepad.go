package main

import (
	"context"
	"log"
	"time"

	"gitlab.com/jkmn/errors"
	"google.golang.org/protobuf/proto"

	"gitlab.com/jkmn/tractor/pkg/common"
	"gitlab.com/jkmn/tractor/pkg/gamepad"
	"gitlab.com/jkmn/tractor/pkg/udp"
)

func sendUpdates(ctx context.Context, ch chan<- []byte, gp *gamepad.Gamepad, s chan struct{}) {

	t := time.NewTicker(50 * time.Millisecond)

	updated := false
	for {
		select {
		case _, ok := <-s:
			if !ok {
				return
			}
			updated = true
		case <-t.C:
			if updated {
				sendUpdate(ch, gp)
				updated = false
			}
		case <-ctx.Done():
			return
		}
	}
}

func sendUpdate(ch chan<- []byte, gp *gamepad.Gamepad) {

	log.Print("sending update")

	b, err := proto.Marshal(gp)
	if err != nil {
		log.Print(errors.Stack(err))
	}

	ch <- b
}

func startGamepad(ctx context.Context) {

	for {
		paths, err := gamepad.Find()
		if err != nil {
			log.Fatal(errors.Stack(err))
		}
		if len(paths) == 0 {
			log.Print("unable to find any connected gamepad")
			time.Sleep(time.Second / 2)
			continue
		}

		log.Print("FOUND DEVICE: ", paths[0])

		gp, s, err := gamepad.StartListening(paths[0])
		if err != nil {
			log.Print(errors.Stack(err))
		}

		ch, err := udp.NewUDPSender(ctx, common.InstanceNameMaster, common.ServiceNameInputSink)
		if err != nil {
			log.Print(errors.Stack(err))
		}

		sendUpdates(ctx, ch, gp, s)

		select {
		case <-ctx.Done():
			return
		default:
			time.Sleep(time.Second / 2)
			log.Print("attempting to reconnect to gamepad.")
		}
	}
}
