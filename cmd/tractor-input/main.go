package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/jkmn/errors"

	"gitlab.com/jkmn/tractor/pkg/common"
	"gitlab.com/jkmn/tractor/pkg/udp"
)

const INSTANCE_NAME string = common.InstanceNameInput

func main() {

	ctx, cancel := context.WithCancel(context.Background())

	heartbeats, err := udp.ReceiveHeartbeats(ctx, INSTANCE_NAME, common.PortHeartbeatInput)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	go updateHeartbeats(heartbeats)

	err = udp.SendHeartbeats(ctx, INSTANCE_NAME, common.InstanceNameMaster)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	go startGamepad(ctx)

	// Clean exit.
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	select {
	case <-sig:
		// Exit by user
		log.Print("shutting down")
		cancel()
		time.Sleep(1 * time.Second)
	}
}
