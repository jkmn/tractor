package relay

import (
	"fmt"
	"log"
)

type Relay []bool

func NewRelay() Relay {
	r := make(Relay, 8)
	return r
}

func (r Relay) Set(pos int, val bool) {
	if len(r)-1 < pos {
		log.Print("relay set position out of bounds")
	}
	r[pos] = val
}

func (r Relay) String() string {
	var s string
	for i := uint(0); i < 8; i++ {
		j := 0
		if r[i] {
			j = 1
		}
		s += fmt.Sprintf("R%d:%d\t", i, j)
	}
	return s
}

func (r Relay) Byte() byte {
	if len(r) != 8 {
		return 0
	}

	var b byte = 0
	for i := uint(0); i < 8; i++ {
		if r[i] {
			b = b | (1 << i)
		}
	}

	return b
}
