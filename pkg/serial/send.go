package serial

import (
	"time"

	"github.com/tarm/serial"
	"gitlab.com/jkmn/errors"
)

func Write(path string, data []byte) error {
	c := &serial.Config{Name: path, Baud: 9600, ReadTimeout: 5 * time.Second}
	s, err := serial.OpenPort(c)
	if err != nil {
		return errors.Stack(err)
	}
	defer s.Close()

	_, err = s.Write(data)
	if err != nil {
		return errors.Stack(err)
	}

	return nil
}
