package serial

import (
	"os"
	"strings"
	"time"

	"github.com/tarm/serial"
	"gitlab.com/jkmn/errors"
)

func FindSerial(id string) (string, error) {
	serials, err := findSerials()
	if err != nil {
		return "", errors.Stack(err)
	}

	for _, serial := range serials {
		id_, err := identifySerial(serial)
		if err != nil {
			return "", errors.Stack(err)
		}

		id_ = strings.TrimSpace(id_)
		if id_ == id {
			return serial, nil
		}
	}

	return "", nil
}

func identifySerial(path string) (string, error) {
	c := &serial.Config{Name: path, Baud: 9600, ReadTimeout: 5 * time.Second}
	s, err := serial.OpenPort(c)
	if err != nil {
		return "", errors.Stack(err)
	}
	defer s.Close()

	_, err = s.Write([]byte("\nID\n"))
	if err != nil {
		return "", errors.Stack(err)
	}

	buf := make([]byte, 128)
	n, err := s.Read(buf)
	return string(buf[:n]), nil
}

func findSerials() ([]string, error) {
	dev, err := os.Open("/dev")
	if err != nil {
		return nil, errors.Stack(err)
	}
	defer dev.Close()

	devs, err := dev.Readdir(-1)
	if err != nil {
		return nil, errors.Stack(err)
	}

	serials := make([]string, 0)

	for _, d := range devs {
		if strings.HasPrefix(d.Name(), "ttyUSB") {
			serials = append(serials, "/dev/"+d.Name())
		}
	}

	return serials, nil
}
