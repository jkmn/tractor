package common

const (
	PortEngine          int = 3202 // 🚂 = U+1F682 or 0o373202
	PortHeartbeatMaster     = 2223 // 💓 = U+1F493 or 0o372223
	PortHeartbeatEngine     = 2224
	PortHeartbeatRelay      = 2225
	PortHeartbeatInput      = 2226
	PortHeartbeatCamera     = 2227
	PortInputSink           = 2571 // 🕹️ = U+1F579 or 0o372571
	PortRelaySink           = 3245 // 🚥 = U+1F6A5 or 0o373245
)
