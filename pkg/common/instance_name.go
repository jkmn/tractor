package common

const (
	InstanceNameMaster string = "master"
	InstanceNameEngine string = "engine"
	InstanceNameRelay  string = "relay"
	InstanceNameInput  string = "input"
	InstanceNameCamera string = "camera"
)
