package common

const (
	ServiceNameHeartbeat  string = "_heartbeat._tractor._udp"
	ServiceNameInputSink         = "_input_sink._tractor._udp"
	ServiceNameRelaySink         = "_relay_sink._tractor._udp"
	ServiceNameEngineSink        = "_engine_sink._tractor._udp"
	ServiceNameCameraSink        = "_camera_sink._tractor._udp"
)
