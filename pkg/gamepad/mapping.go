package gamepad

import (
	"fmt"
	"log"

	evdev "github.com/gvalkov/golang-evdev"
)

/*
The Logitech Gamepad F310 was the first device that I implemented. As a result,
it's our reference device. Every other device maps to an approximation of this
first device.
*/

type mapping struct {
	inputs  map[string]evdev.InputEvent
	scalers map[string]scaler
}

type scaler func(int32) int32

func mappingKey(ev *evdev.InputEvent) string {
	return fmt.Sprintf("c%03dt%02d", ev.Code, ev.Type)
}

func remap(ev *evdev.InputEvent, mapping mapping) *evdev.InputEvent {

	ev_, ok := mapping.inputs[mappingKey(ev)]
	if !ok {
		log.Printf("unmapped event: %s", ev)
		return nil
	}

	blank := evdev.InputEvent{}
	if ev_ == blank {
		return nil
	}

	ev_.Time = ev.Time
	ev_.Value = ev.Value

	s, ok := mapping.scalers[mappingKey(ev)]
	if ok {
		ev_.Value = s(ev.Value)
	}

	return &ev_
}

var mappings map[string]mapping = map[string]mapping{
	// Logitech Gamepad F310
	"046d:c21d": mapping{
		inputs: map[string]evdev.InputEvent{
			"c000t00": {},                    // ignore
			"c000t03": {Code: 000, Type: 03}, // Left Stick X
			"c001t03": {Code: 1, Type: 03},   // Left Stick Y
			"c002t03": {Code: 2, Type: 03},   // LT -> LT
			"c003t03": {Code: 3, Type: 03},   // Right Stick X
			"c004t03": {Code: 4, Type: 03},   // Right Stick Y
			"c005t03": {Code: 5, Type: 03},   // RT -> RT
			"c016t03": {Code: 16, Type: 03},  // DPAD X
			"c017t03": {Code: 17, Type: 03},  // DPAD Y
			"c304t01": {Code: 304, Type: 01}, // A -> A
			"c305t01": {Code: 305, Type: 01}, // B -> B
			"c307t01": {Code: 307, Type: 01}, // X -> X
			"c308t01": {Code: 308, Type: 01}, // Y -> Y
			"c310t01": {Code: 310, Type: 01}, // LB -> LB
			"c311t01": {Code: 311, Type: 01}, // RB -> RB
			"c314t01": {Code: 314, Type: 01}, // Back -> Back
			"c315t01": {Code: 315, Type: 01}, // Start -> Start
			"c316t01": {Code: 316, Type: 01}, // Home -> Home
			"c317t01": {Code: 317, Type: 01}, // LZ -> LZ
			"c318t01": {Code: 318, Type: 01}, // RZ -> RZ
		},
	},
	// Logitech Dual Action
	"046d:c216": mapping{
		inputs: map[string]evdev.InputEvent{
			"c000t00": {},                    // ignore
			"c000t03": {Code: 000, Type: 03}, // Left Stick X
			"c001t03": {Code: 1, Type: 03},   // Left Stick Y
			"c002t03": {Code: 3, Type: 03},   // Right Stick X
			"c004t04": {},                    // ignore
			"c005t03": {Code: 4, Type: 03},   // Right Stick Y
			"c016t03": {Code: 16, Type: 03},  // DPAD X
			"c017t03": {Code: 17, Type: 03},  // DPAD Y
			"c288t01": {Code: 307, Type: 01}, // 1 -> X
			"c289t01": {Code: 304, Type: 01}, // 2 -> A
			"c290t01": {Code: 305, Type: 01}, // 4 -> B
			"c291t01": {Code: 308, Type: 01}, // 4 -> Y
			"c292t01": {Code: 310, Type: 01}, // 5 -> LB
			"c293t01": {Code: 311, Type: 01}, // 6 -> RB
			"c294t01": {Code: 2, Type: 03},   // 7 -> LT
			"c295t01": {Code: 5, Type: 03},   // 8 -> RT
			"c296t01": {Code: 314, Type: 01}, // 9 -> Back
			"c297t01": {Code: 315, Type: 01}, // 10 -> Start
			"c298t01": {Code: 317, Type: 01}, // LZ -> LZ
			"c299t01": {Code: 318, Type: 01}, // LZ -> RZ
		},
		scalers: map[string]scaler{
			"c000t03": func(in int32) int32 { return in*256 - 32768 }, // Left Stick X
			"c001t03": func(in int32) int32 { return in*256 - 32768 }, // Left Stick Y
			"c002t03": func(in int32) int32 { return in*256 - 32768 }, // Right Stick X
			"c005t03": func(in int32) int32 { return in*256 - 32768 }, // Right Stick Y
			"c294t01": func(in int32) int32 { return in * 255 },       // 7 -> LT
			"c295t01": func(in int32) int32 { return in * 255 },       // 8 -> RT
		},
	},
}

func deviceKey(dev *evdev.InputDevice) string {
	return fmt.Sprintf("%04x:%04x", dev.Vendor, dev.Product)
}
