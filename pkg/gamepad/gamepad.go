// go:generate protoc --proto_path=src --go_out=build/gen --go_opt=paths=source_relative src/foo.proto src/bar/baz.proto
package gamepad

import (
	"log"

	evdev "github.com/gvalkov/golang-evdev"
)

/*
	[15241.928249] usb 2-1.4.7: new full-speed USB device number 14 using ehci-pci
	[15242.060635] usb 2-1.4.7: NewGamepad USB device found, idVendor=046d, idProduct=c21d, bcdDevice=40.14
	[15242.060638] usb 2-1.4.7: NewGamepad USB device strings: Mfr=1, Product=2, SerialNumber=3
	[15242.060639] usb 2-1.4.7: Product: Gamepad F310
	[15242.060641] usb 2-1.4.7: Manufacturer: Logitech
	[15242.060642] usb 2-1.4.7: SerialNumber: C91A35A1
	[15242.614796] input: Logitech Gamepad F310 as /devices/pci0000:00/0000:00:1d.0/usb2/2-1/2-1.4/2-1.4.7/2-1.4.7:1.0/input/input22
	[15242.615097] usbcore: registered new interface driver xpad
*/

/*
	Logitech Gamepad F310

	Capabilities:
	EV_KEY (01)
	EV_ABS (03)

	stick left X:	code REL_X (00), type 03 (-32768,32768)
	stick left Y:	code REL_Y (01), type 03 (-32768,32768)
	trigger left:	code REL_Z (02), type 03 (0,255)
	stick right X:	code REL_RX (03), type 03 (-32768,32768)
	stick right Y:	code REL_RY (04), type 03 (-32768,32768)
	trigger right:	code REL_RZ (05), type 03 (0,255)
	DPAD X:			code 16, type 03 [-1,0,1]
	DPAD Y:			code 17, type 03 [-1,0,1]
	button A: 		code 304, type 01 [0,1]
	button B:		code 305, type 01 [0,1]
	button X:		code 307, type 01 [0,1]
	button Y:		code 308, type 01 [0,1]
	button LB:		code 310, type 01 [0,1]
	button RB:		code 311, type 01 [0,1]
	button back:	code 314, type 01 [0,1]
	button start:	code 315, type 01 [0,1]
	stick left Z:	code 317, type 01 [0,1]
	stick right Z:	code 318, type 01 [0,1]
*/

func NewGamepad() *Gamepad {

	var (
		_bool  bool  = false
		_int32 int32 = 0
	)

	gp := &Gamepad{
		StickLeft:    NewStick(),
		StickRight:   NewStick(),
		Dpad:         NewDPad(),
		TriggerLeft:  &_int32,
		TriggerRight: &_int32,
		ButtonA:      &_bool,
		ButtonB:      &_bool,
		ButtonX:      &_bool,
		ButtonY:      &_bool,
		ButtonLB:     &_bool,
		ButtonRB:     &_bool,
		ButtonLZ:     &_bool,
		ButtonRZ:     &_bool,
		ButtonStart:  &_bool,
		ButtonHome:   &_bool,
		ButtonBack:   &_bool,
	}

	return gp
}

func NewStick() *Gamepad_Stick {
	var _int32 int32 = 128

	stick := &Gamepad_Stick{
		X: &_int32,
		Y: &_int32,
	}

	return stick
}

func NewDPad() *Gamepad_Stick {
	var _int32 int32 = 0

	stick := &Gamepad_Stick{
		X: &_int32,
		Y: &_int32,
	}

	return stick
}

func (gp *Gamepad) update(ev *evdev.InputEvent) {
	switch ev.Type {
	case 00:
	//ignore
	case 01:
		kev := evdev.NewKeyEvent(ev)
		gp.updateKey(kev)
	case 03:
		rev := evdev.NewRelEvent(ev)
		gp.updateAbs(rev)
	}
}

func (gp *Gamepad) updateKey(ev *evdev.KeyEvent) {

	value := ev.State == evdev.KeyDown

	switch ev.Scancode {
	case 304: // a
		gp.ButtonA = &value
	case 305: // b
		gp.ButtonB = &value
	case 307: // x
		gp.ButtonX = &value
	case 308: // y
		gp.ButtonY = &value
	case 310: // lb
		gp.ButtonLB = &value
	case 311: // rb
		gp.ButtonRB = &value
	case 314: // back
		gp.ButtonBack = &value
	case 315: // start
		gp.ButtonStart = &value
	case 316: // stick right
		gp.ButtonHome = &value
	case 317: // stick left
		gp.ButtonLZ = &value
	case 318: // stick right
		gp.ButtonRZ = &value
	default:
		log.Print(ev.String())
	}
}

func (gp *Gamepad) updateAbs(ev *evdev.RelEvent) {

	value := ev.Event.Value

	switch ev.Event.Code {
	case 16: // DPAD X
		gp.Dpad.X = &value
	case 17: // DPAD Y
		gp.Dpad.Y = &value
	case evdev.REL_RX:
		gp.StickRight.X = &value
	case evdev.REL_RY:
		gp.StickRight.Y = &value
	case evdev.REL_RZ:
		gp.TriggerRight = &value
	case evdev.REL_X:
		gp.StickLeft.X = &value
	case evdev.REL_Y:
		gp.StickLeft.Y = &value
	case evdev.REL_Z:
		gp.TriggerLeft = &value
	default:
		log.Printf("UNKNOWN REL EVENT: %s (%s)", ev, ev.Event)
	}
}
