package gamepad

import (
	"log"

	evdev "github.com/gvalkov/golang-evdev"
	"gitlab.com/jkmn/errors"
)

func StartListening(path string) (*Gamepad, chan struct{}, error) {

	dev, err := evdev.Open(path)
	if err != nil {
		return nil, nil, errors.Stack(err)
	}

	err = dev.Grab()
	if err != nil {
		return nil, nil, errors.Stack(err)
	}

	signal := make(chan struct{})

	pad := NewGamepad()

	mapping, ok := mappings[deviceKey(dev)]
	if !ok {
		return nil, nil, errors.Newf("mapping not found for device: %s (%s)", deviceKey(dev), dev.Name)
	}

	go listen(pad, dev, mapping, signal)

	return pad, signal, nil
}

// Takes all events published updates the abstracting
// GamePad object.
func listen(pad *Gamepad, dev *evdev.InputDevice, mapping mapping, signal chan struct{}) {
	log.Print("creating go routine for ", dev.Name)

	var (
		err error
		ev  *evdev.InputEvent
	)

	for ev, err = dev.ReadOne(); err == nil; ev, err = dev.ReadOne() {

		ev = remap(ev, mapping)
		if ev == nil {
			continue
		}

		pad.update(ev)

		select {
		case signal <- struct{}{}:
		default:
		}
	}

	log.Print(errors.Stack(err))

	close(signal)

	return
}
