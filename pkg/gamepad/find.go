package gamepad

import (
	evdev "github.com/gvalkov/golang-evdev"
	"gitlab.com/jkmn/errors"
)

func Find() ([]string, error) {

	devices, err := evdev.ListInputDevices()
	if err != nil {
		return nil, errors.Stack(err)
	}

	joys := make([]string, 0)
	for _, dev := range devices {
		// These are the vendor and device codes of the Logitech F310.
		// The Logitech F310 should only have one virtual device per physical device.
		if !(dev.Vendor == 0x046d && dev.Product == 0xc21d) &&
			!(dev.Vendor == 0x046d && dev.Product == 0xc216) {
			continue
		}

		joys = append(joys, dev.Fn)
	}

	if len(joys) == 0 {
		return nil, nil
	}

	return joys, nil
}
