package udp

import (
	"context"
	"log"
	"net"

	"github.com/grandcat/zeroconf"
	"gitlab.com/jkmn/errors"
)

func NewUDPReceiver(ctx context.Context, instanceName, serviceName string, port int) (<-chan []byte, error) {

	ch := make(chan []byte)

	server, err := zeroconf.Register(
		instanceName,
		serviceName,
		"local.",
		port,
		[]string{"txtv=0", "lo=1", "la=2"},
		nil,
	)
	if err != nil {
		return nil, errors.Stack(err)
	}

	conn, err := net.ListenUDP("udp", &net.UDPAddr{Port: port})
	if err != nil {
		return nil, errors.Stack(err)
	}

	go udpListen(ctx, server, conn, ch)

	return ch, nil
}

func udpListen(ctx context.Context, server *zeroconf.Server, conn *net.UDPConn, ch chan<- []byte) {

	defer server.Shutdown()
	defer conn.Close()

	for {
		select {
		case <-ctx.Done():
			close(ch)
			return
		default:
		}

		b := make([]byte, 2096)

		n, err := conn.Read(b)
		if err != nil {
			log.Print(errors.Stack(err))
		}

		ch <- b[:n]
	}
}
