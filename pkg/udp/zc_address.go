package udp

import (
	"net"

	"github.com/grandcat/zeroconf"
)

func GetAddress(entry *zeroconf.ServiceEntry) *net.UDPAddr {

	if entry == nil {
		return nil
	}

	if len(entry.AddrIPv6) > 0 {
		return &net.UDPAddr{IP: entry.AddrIPv6[0], Port: entry.Port}
	} else if len(entry.AddrIPv4) > 0 {
		return &net.UDPAddr{IP: entry.AddrIPv4[0], Port: entry.Port}
	}

	return nil
}
