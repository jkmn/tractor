package udp

import (
	"context"
	"time"

	"gitlab.com/jkmn/errors"

	"gitlab.com/jkmn/tractor/pkg/common"
)

func SendHeartbeats(ctx context.Context, srcInstance string, dstInstance string) error {

	ctx, cancel := context.WithCancel(ctx)

	ch, err := NewUDPSender(ctx, dstInstance, common.ServiceNameHeartbeat)
	if err != nil {
		cancel()
		return errors.Stack(err)
	}

	go sendHeartbeats(ctx, srcInstance, ch)

	return nil
}

func sendHeartbeats(ctx context.Context, srcInstance string, ch chan<- []byte) {
	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			ch <- []byte(srcInstance)
		case <-ctx.Done():
			return
		}
	}
}
