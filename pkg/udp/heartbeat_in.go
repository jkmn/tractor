package udp

import (
	"context"

	"gitlab.com/jkmn/errors"

	"gitlab.com/jkmn/tractor/pkg/common"
)

func ReceiveHeartbeats(ctx context.Context, instanceName string, port int) (<-chan []byte, error) {
	ch, err := NewUDPReceiver(ctx, instanceName, common.ServiceNameHeartbeat, port)
	if err != nil {
		return nil, errors.Stack(err)
	}

	return ch, nil
}
