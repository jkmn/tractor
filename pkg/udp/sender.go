package udp

import (
	"context"
	"log"
	"net"

	"github.com/grandcat/zeroconf"
	"gitlab.com/jkmn/errors"
)

func NewUDPSender(ctx context.Context, dstInstance string, serviceName string) (chan<- []byte, error) {

	resolver, err := zeroconf.NewResolver(nil)
	if err != nil {
		return nil, errors.Stack(err)
	}

	entries := make(chan *zeroconf.ServiceEntry)

	// it doesn't appear that we need to keep this running in a thread.
	// set it and forget it.
	err = resolver.Lookup(ctx, dstInstance, serviceName, "local.", entries)
	if err != nil {
		return nil, errors.Stack(err)
	}

	ch := make(chan []byte)

	go sendUdp(ctx, entries, dstInstance, ch)

	return ch, nil
}

func sendUdp(ctx context.Context, entries chan *zeroconf.ServiceEntry, dstInstance string, ch chan []byte) {
	/*
		The zeroconf resolver doesn't filter on instance name (at least at the
		time of this writing). Therefore, we need to filter on address updates.
	*/

	var addr *net.UDPAddr

	for {
		select {
		case b, ok := <-ch:
			if !ok {
				return
			}
			err := sendRaw(addr, b)
			if err != nil {
				log.Print(errors.Stack(err))
			}
		case entry := <-entries:
			if entry == nil {
				continue
			}
			if entry.Instance != dstInstance {
				continue
			}
			addr = GetAddress(entry)
			log.Printf("discovered address for sending: %s", addr)
		case <-ctx.Done():
			close(ch)
			return
		}
	}
}

func sendRaw(addr *net.UDPAddr, b []byte) error {
	if addr == nil {
		return nil
	}

	conn, err := net.DialUDP("udp", nil, addr)
	if err != nil {
		return errors.Stack(err)
	}
	defer conn.Close()

	_, err = conn.Write(b)
	if err != nil {
		return errors.Stack(err)
	}

	return nil
}
