DEMOS=arduino-count arduino-count-2 arduino-input arduino-input-2
BINS=tractor-input tractor-master tractor-engine tractor-relay tractor-camera

.PHONY: relays clean setup $(BINS) $(DEMOS) all demos

all: $(BINS)

demos: $(DEMOS)

tmux: TMUX_NAME="tractor"
tmux:
	-tmux kill-session -t $(TMUX_NAME)
	tmux new-session -d -s $(TMUX_NAME)
	tmux set-option -g -t $(TMUX_NAME) remain-on-exit on
	tmux bind-key r respawn-pane -k
	tmux bind-key k kill-session
#	echo $(BINS) | tr " " "\n" | xargs -t -I '{}' echo {}
#	echo $(BINS) | tr " " "\n" | xargs -t -I '{}' $(MAKE) {} && tmux new-window -t $(TMUX_NAME) -n {} ./{}
	tmux new-window -t $(TMUX_NAME) -n master "$(MAKE) tractor-master && ./tractor-master"
	tmux new-window -t $(TMUX_NAME) -n input "$(MAKE) tractor-input && ./tractor-input"
	tmux new-window -t $(TMUX_NAME) -n relay "$(MAKE) tractor-relay && ./tractor-relay"
	tmux new-window -t $(TMUX_NAME) -n engine "$(MAKE) tractor-engine && ./tractor-engine"
	tmux new-window -t $(TMUX_NAME) -n camera "$(MAKE) tractor-camera && ./tractor-camera"
	tmux select-window -t $(TMUX_NAME):0
	tmux attach-session -t $(TMUX_NAME)

tractor-input:
	go build gitlab.com/jkmn/tractor/cmd/tractor-input

tractor-master:
	go build gitlab.com/jkmn/tractor/cmd/tractor-master

tractor-engine:
	go build gitlab.com/jkmn/tractor/cmd/tractor-engine

tractor-relay:
	go build gitlab.com/jkmn/tractor/cmd/tractor-relay

tractor-camera:
	go build gitlab.com/jkmn/tractor/cmd/tractor-camera

# set up the environment for building, etc.
setup:
	curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | BINDIR=$(HOME)/opt/bin sh
	arduino-cli core update-index
	arduino-cli core install arduino:avr
	go get -u golang.org/x/tools/cmd/goimports
	go get -u google.golang.org/protobuf/...

relay0:
	arduino-cli compile --fqbn arduino:avr:nano arduino/relay0
	arduino-cli upload -p /dev/ttyUSB0 --fqbn arduino:avr:nano arduino/relay0

relay1:
	arduino-cli compile --fqbn arduino:avr:nano arduino/relay1
	arduino-cli upload -p /dev/ttyUSB1 --fqbn arduino:avr:nano arduino/relay1

clean:
	rm -rf $(BINS)
	rm -rf $(DEMOS)
	rm -rf arduino/relay0/*.elf
	rm -rf arduino/relay0/*.hex
	rm -rf arduino/relay1/*.elf
	rm -rf arduino/relay1/*.hex

demo-count: arduino-count
	$(PWD)/arduino-count

demo-count-2: arduino-count-2
	$(PWD)/arduino-count-2

demo-input: arduino-input
	$(PWD)/arduino-input

demo-input-2: arduino-input-2
	$(PWD)/arduino-input-2

arduino-count:
	go build gitlab.com/jkmn/tractor/demo/arduino-count

arduino-count-2:
	go build gitlab.com/jkmn/tractor/demo/arduino-count-2

arduino-input:
	go build gitlab.com/jkmn/tractor/demo/arduino-input

arduino-input-2:
	go build gitlab.com/jkmn/tractor/demo/arduino-input-2

fmt:
	for d in $(shell go list -f {{.Dir}} ./...); do goimports -w $$d/*.go; done
	git status

proto:
