# DIY Tractor

Code and documentation of a tractor that I'm building. I couldn't bring myself 
to go in debt for a new tractor, and, at least where I live, any used working 
tractor is highly coveted. In other words, the secondary market for tractors is 
virtually non-existent.

The goal is to create a modular tractor that is easy to operate and maintain and 
accomplishes the work that needs to be done on my modest property.

As of right now, I aim to include the following features:
* Assembled primarily from salvaged materials and parts
* Easily removable power plant
* Fully electronic control
* Fully-enclosed cab for safety
* Hydrostatic drive
* Industry standard control patterns
* On-board network for inter-module communication and control

**Most documentation has moved out of the README and into the 
[wiki](https://gitlab.com/jkmn/tractor/-/wikis/home).**

If anyone is interested in this project or has questions, feel free to comment
on my [Bitchute videos](https://www.bitchute.com/channel/SWfGKgOBIGue/) or
contact me through email (jackman@jackmanlabs.com). I'm happy to answer
questions and receive suggestions.

Early CAD drawings:

![front view](photos/tractor_front_angle.png "early concept front view")
![side view](photos/tractor_side.png "early concept side view")

## Repo Organization

The repo is organized as a
[standard](https://github.com/golang-standards/project-layout) Go repo. Because
this project is a hobby project, expect a lot of spontaneous refactoring.

## Safety

Please note that I am making this repo public as a means to assist people with
their own projects. This resource is not intended to be comprehensive nor a 
how-to guide on making industrial machinery. I urge you to take your safety 
seriously, do your research, and think critically about what you're doing.

Professionally, I am a programmer with one eye. Should anything happen to my eye
or hands, I would be out of work. Therefore, I take my safety very seriously.

Use proper safety equipment. Learn how to use your tools safely and properly. If
you are inexperienced with the tools, I suggest you take a course from a local
technical college. They often allow people to take courses a-la-carte and they
are very affordable. 