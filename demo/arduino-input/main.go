package main

import (
	"log"
	"time"

	"gitlab.com/jkmn/errors"

	"gitlab.com/jkmn/tractor/pkg/gamepad"
	"gitlab.com/jkmn/tractor/pkg/relay"
	"gitlab.com/jkmn/tractor/pkg/serial"
)

func main() {
	paths, err := gamepad.Find()
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	if len(paths) == 0 {
		log.Fatal("unable to find a connected gamepad")
	}

	pad, _, err := gamepad.StartListening(paths[0])
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	path, err := serial.FindSerial("RELAY0")
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	if path == "" {
		log.Fatal("RELAY0 not found.")
	}

	// At this point, we have a GamePad object that is being constantly updated
	// in the background. We can trust that the GamePad is always as up-to-date
	// as it can be.

	// I will eventually make this more abstract. Right now I just want to get it working.

	t := time.NewTicker(time.Millisecond * 50)
	r := relay.NewRelay()
	var prevByte byte = 1
	for range t.C {
		//log.Print("tick")
		r.Set(0, pad.Dpad.GetY() > 0)
		r.Set(1, pad.Dpad.GetY() < 0)
		r.Set(2, pad.Dpad.GetX() > 0)
		r.Set(3, pad.Dpad.GetX() < 0)
		r.Set(4, pad.StickRight.GetY() > 10000)
		r.Set(5, pad.StickRight.GetY() < -10000)
		r.Set(6, pad.StickRight.GetX() > 10000)
		r.Set(7, pad.StickRight.GetX() < -10000)

		//log.Print(r.String())

		if r.Byte() != prevByte {
			log.Printf("%08b", r.Byte())
			err = serial.Write(path, []byte{r.Byte(), '\n'})
			if err != nil {
				log.Fatal(errors.Stack(err))
			}
			prevByte = r.Byte()
		}
	}
}
