package main

import (
	"bytes"
	"encoding/json"
	"log"

	"gitlab.com/jkmn/errors"

	"gitlab.com/jkmn/tractor/pkg/gamepad"
)

func main() {

	paths, err := gamepad.Find()
	if err != nil {
		log.Fatal(errors.Stack(err))
	}
	if len(paths) == 0 {
		log.Fatal("unable to find any connected gamepad")
	}

	log.Print("FOUND DEVICE: ", paths[0])

	gp, signal, err := gamepad.StartListening(paths[0])
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	for range signal {
		buf := bytes.NewBuffer(nil)
		enc := json.NewEncoder(buf)
		err := enc.Encode(gp)
		if err != nil {
			log.Fatal(errors.Stack(err))
		}

		log.Print(buf.Len())
	}
}
