package main

import (
	"log"
	"time"

	"gitlab.com/jkmn/errors"

	"gitlab.com/jkmn/tractor/pkg/gamepad"
	"gitlab.com/jkmn/tractor/pkg/relay"
	"gitlab.com/jkmn/tractor/pkg/serial"
)

func main() {
	paths, err := gamepad.Find()
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	if len(paths) == 0 {
		log.Fatal("unable to find a connected gamepad")
	}

	pad, _, err := gamepad.StartListening(paths[0])
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	rPath0, err := serial.FindSerial("RELAY0")
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	if rPath0 == "" {
		log.Fatal("RELAY0 not found.")
	}

	rPath1, err := serial.FindSerial("RELAY1")
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	if rPath1 == "" {
		log.Fatal("RELAY1 not found.")
	}

	// At this point, we have a GamePad object that is being constantly updated
	// in the background. We can trust that the GamePad is always as up-to-date
	// as it can be.

	// I will eventually make this more abstract. Right now I just want to get it working.

	t := time.NewTicker(time.Millisecond * 50)
	r0 := relay.NewRelay()
	r1 := relay.NewRelay()
	var prevByte0 byte = 0
	var prevByte1 byte = 0
	for range t.C {
		//log.Print("tick")
		r0.Set(0, pad.StickLeft.GetY() > 1000)
		r0.Set(1, pad.StickLeft.GetY() < -1000)
		r0.Set(2, pad.StickLeft.GetX() > 1000)
		r0.Set(3, pad.StickLeft.GetX() < -1000)
		r0.Set(4, pad.StickRight.GetY() > 10000)
		r0.Set(5, pad.StickRight.GetY() < -10000)
		r0.Set(6, pad.StickRight.GetX() > 10000)
		r0.Set(7, pad.StickRight.GetX() < -10000)

		r1.Set(0, pad.Dpad.GetY() > 0)
		r1.Set(1, pad.Dpad.GetY() < 0)
		r1.Set(2, pad.Dpad.GetX() > 0)
		r1.Set(3, pad.Dpad.GetX() < 0)
		r1.Set(4, pad.GetButtonA())
		r1.Set(5, pad.GetButtonY())
		r1.Set(6, pad.GetButtonB())
		r1.Set(7, pad.GetButtonX())

		//log.Print(r.String())

		if r0.Byte() != prevByte0 {
			err = serial.Write(rPath0, []byte{r0.Byte(), '\n'})
			if err != nil {
				log.Fatal(errors.Stack(err))
			}
			prevByte0 = r0.Byte()
		}

		if r1.Byte() != prevByte1 {
			err = serial.Write(rPath1, []byte{r1.Byte(), '\n'})
			if err != nil {
				log.Fatal(errors.Stack(err))
			}
			prevByte1 = r1.Byte()
		}
	}
}
