package main

import (
	"log"
	"time"

	"gitlab.com/jkmn/errors"

	"gitlab.com/jkmn/tractor/pkg/serial"
)

func main() {
	path, err := serial.FindSerial("RELAY0")
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	if path == "" {
		log.Fatal("RELAY0 not found.")
	}

	err = count(path)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}
}

func count(path string) error {

	var err error

	for i := byte(0); i < 255; i++ {

		err = serial.Write(path, []byte{i, '\n'})
		if err != nil {
			return errors.Stack(err)
		}

		time.Sleep(time.Second / 10)
	}

	err = serial.Write(path, []byte{0, '\n'})
	if err != nil {
		return errors.Stack(err)
	}

	return nil
}
