package main

import (
	"log"
	"time"

	"github.com/stianeikeland/go-rpio"
)

const (
	LED0 rpio.Pin = 6
	LED1          = 13
	LED2          = 19
	LED3          = 26
)

func main() {
	err := rpio.Open()
	if err != nil {
		log.Fatal(err)
	}
	defer rpio.Close()

	rpio.PinMode(LED0, rpio.Output)
	rpio.PinMode(LED1, rpio.Output)
	rpio.PinMode(LED2, rpio.Output)
	rpio.PinMode(LED3, rpio.Output)

	rpio.WritePin(LED0, rpio.High)
	time.Sleep(time.Second)
	rpio.WritePin(LED0, rpio.Low)

	rpio.WritePin(LED1, rpio.High)
	time.Sleep(time.Second)
	rpio.WritePin(LED1, rpio.Low)

	rpio.WritePin(LED2, rpio.High)
	time.Sleep(time.Second)
	rpio.WritePin(LED2, rpio.Low)

	rpio.WritePin(LED3, rpio.High)
	time.Sleep(time.Second)
	rpio.WritePin(LED3, rpio.Low)

}
