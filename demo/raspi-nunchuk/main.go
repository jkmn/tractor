package main

import (
	"fmt"
	"log"
	"math"
	"time"

	"github.com/d2r2/go-i2c"
	"github.com/d2r2/go-logger"
	"gitlab.com/jkmn/errors"
)

const (
	// Calibration accelerometer values, depends on your Nunchuk
	NUNCHUK_ACCEL_X_ZERO int = 512
	NUNCHUK_ACCEL_Y_ZERO int = 512
	NUNCHUK_ACCEL_Z_ZERO int = 512

	// Calibration joystick values
	NUNCHUK_JOYSTICK_X_ZERO int = 127
	NUNCHUK_JOYSTICK_Y_ZERO int = 128

	DISABLE_ENCRYPTION bool = false
)

func main() {
	err := logger.ChangePackageLogLevel("i2c", logger.InfoLevel)
	if err != nil {
		log.Fatal(err)
	}

	iic, err := i2c.NewI2C(0x52, 1)
	if err != nil {
		log.Fatal(err)
	}
	defer iic.Close()

	err = nunchukInit(iic)
	if err != nil {
		log.Fatal(err)
	}

	for {

		data, err := nunchukUpdate(iic)
		if err != nil {
			log.Print(err)
			continue
		}

		var (
			z         bool = ^data[5]>>0&1 == 1
			c         bool = ^data[5]>>1&1 == 1
			joyXRaw   int  = data[0]
			joyYRaw   int  = data[1]
			joyX      int  = joyXRaw - NUNCHUK_JOYSTICK_X_ZERO
			joyY      int  = joyYRaw - NUNCHUK_JOYSTICK_Y_ZERO
			accelXRaw int  = data[2]<<2 | ((data[5] >> 2) & 3)
			accelYRaw int  = data[3]<<2 | ((data[5] >> 4) & 3)
			accelZRaw int  = data[4]<<2 | ((data[5] >> 6) & 3)
			accelX    int  = accelXRaw - NUNCHUK_ACCEL_X_ZERO
			accelY    int  = accelYRaw - NUNCHUK_ACCEL_Y_ZERO
			accelZ    int  = accelZRaw - NUNCHUK_ACCEL_Z_ZERO
		)
		fmt.Printf("Z:%t C:%t JX:%d JY:%d AX:%d AY:%d AZ:%d\n", z, c, joyX, joyY, accelX, accelY, accelZ)
		fmt.Printf("PITCH:%0.2f ROLL:%0.2f \n", pitch(accelY, accelZ), roll(accelX, accelZ))

		_, err = iic.WriteBytes([]byte{0x00})
		if err != nil {
			continue
		}

		time.Sleep(time.Millisecond * 100)
	}
}

func nunchukUpdate(iic *i2c.I2C) ([]int, error) {

	var dataBytes []byte = make([]byte, 6)
	n, err := iic.ReadBytes(dataBytes)
	if err != nil {
		return nil, errors.Stack(err)
	}

	if n != 6 {
		log.Fatalf("Only read %d bytes!", n)
	}

	var data []int = make([]int, 6)
	for i, b := range dataBytes {
		data[i] = decode(b)
	}

	return data, nil
}

func nunchukInit(iic *i2c.I2C) error {

	var err error

	if DISABLE_ENCRYPTION {

		_, err = iic.WriteBytes([]byte{0xf0, 0x55})
		if err != nil {
			return errors.Stack(err)
		}

		time.Sleep(time.Millisecond * 10)

		_, err = iic.WriteBytes([]byte{0xfb, 0x00})
		if err != nil {
			return errors.Stack(err)
		}
	} else {
		_, err = iic.WriteBytes([]byte{0x40, 0x00})
		if err != nil {
			return errors.Stack(err)
		}
	}

	time.Sleep(time.Millisecond * 10)

	// read the ident from the nunchuk.
	// 0xA4200000 for Nunchuck, 0xA4200101 for Classic, 0xA4200402 for Balance

	_, err = iic.WriteBytes([]byte{0xfa})
	if err != nil {
		return errors.Stack(err)
	}

	time.Sleep(time.Millisecond * 10)

	var ident []byte = make([]byte, 6)
	_, err = iic.ReadBytes(ident)
	if err != nil {
		return errors.Stack(err)
	}

	log.Printf("IDENT: %x", ident)

	return nil
}

func pitch(y, z int) float64 {
	return math.Atan2(float64(y), float64(z)) * 180 / math.Pi
}

func roll(x, z int) float64 {
	return math.Atan2(float64(x), float64(z)) * 180 / math.Pi
}

func decode(b byte) int {
	if DISABLE_ENCRYPTION {
		return int(b)
	}
	return int((b ^ 0x17) + 0x17)
}
