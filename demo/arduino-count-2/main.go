package main

import (
	"log"
	"time"

	"gitlab.com/jkmn/errors"

	"gitlab.com/jkmn/tractor/pkg/serial"
)

func main() {
	rPath0, err := serial.FindSerial("RELAY0")
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	if rPath0 == "" {
		log.Fatal("RELAY0 not found.")
	}

	rPath1, err := serial.FindSerial("RELAY1")
	if err != nil {
		log.Fatal(errors.Stack(err))
	}

	if rPath1 == "" {
		log.Fatal("RELAY1 not found.")
	}

	err = count(rPath0, rPath1)
	if err != nil {
		log.Fatal(errors.Stack(err))
	}
}

func count(path0, path1 string) error {

	var err error

	for i := byte(0); i < 255; i++ {

		err = serial.Write(path0, []byte{i, '\n'})
		if err != nil {
			return errors.Stack(err)
		}

		err = serial.Write(path1, []byte{255 - i, '\n'})
		if err != nil {
			return errors.Stack(err)
		}

		time.Sleep(time.Second / 20)
	}

	err = serial.Write(path0, []byte{0, '\n'})
	if err != nil {
		return errors.Stack(err)
	}

	err = serial.Write(path1, []byte{0, '\n'})
	if err != nil {
		return errors.Stack(err)
	}

	return nil
}
