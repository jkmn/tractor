#define BAUDRATE 9600
#define ID "RELAY0"

char data[100];

void setup()
{
  // Setup relay pins
  for (int i = 0; i < 8; i++) {
    int pin = i + 4;
    pinMode(pin, OUTPUT);
    digitalWrite(pin, HIGH);
  }

  // Initialize serial port:
  Serial.begin(BAUDRATE);
  while (!Serial);
}

void loop()
{
  String newLine = "";
  byte incomingByte = -1;

  while (incomingByte != 10) {
    if (Serial.available() > 0) {
      // read the incoming byte:
      incomingByte = Serial.read();
      newLine = newLine + char(incomingByte);
    }
  }

  if (newLine.length() == 2) {
    activateRelays(newLine[0]);
  } else if (newLine == "ID\n") {
    Serial.println(ID);
  }
}

void activateRelays(byte b) {
  for (int i = 0; i < 8; i++) {
    int pin = i + 4;
    byte mask = 1 << i;

    if (unmask(b, mask)) {
      digitalWrite(pin, LOW);
    } else {
      digitalWrite(pin, HIGH);
    }
  }
}

bool unmask(byte b, byte mask) {
  bool matched = b & mask;
  return matched;
}

void disactivateRelays() {
  for (int i = 0; i < 8; i++) {
    int pin = i + 4;
    digitalWrite(pin, HIGH);
  }
}
