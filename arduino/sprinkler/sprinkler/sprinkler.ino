//#define BAUDRATE 9600
#define INTERVAL 600000 // ten minutes
//#define INTERVAL 60000 // one minute

//char data[100];

void setup()
{
  // Setup relay pins
  for (int i = 0; i < 8; i++) {
    int pin = i + 4;
    pinMode(pin, OUTPUT);
    digitalWrite(pin, HIGH);
  }

  // Initialize serial port:
  //  Serial.begin(BAUDRATE);
  //  while (!Serial);
}

void loop()
{
  unsigned long m = millis();
  unsigned long intervals = m / INTERVAL;
  byte b = 1 << intervals % 8;

  //  sprintf(data, "m: %d\tintervals: %d\tb: %d", m, intervals, b);
  //  Serial.println(data);
  activateRelays(b);

  delay(1000);
}

void activateRelays(byte b) {
  for (int i = 0; i < 8; i++) {
    int pin = i + 4;
    byte mask = 1 << i;

    if (unmask(b, mask)) {
      digitalWrite(pin, LOW);
    } else {
      digitalWrite(pin, HIGH);
    }
  }
}

bool unmask(byte b, byte mask) {
  bool matched = b & mask;
  return matched;
}

void disactivateRelays() {
  for (int i = 0; i < 8; i++) {
    int pin = i + 4;
    digitalWrite(pin, HIGH);
  }
}
