#!/bin/bash

echo SOURCE: "$1"
echo DESTINATION: "$2"

if [[ ! -d $1 ]]; then
  echo "first argument, the source directory, is not a directory."
  exit 1
fi

if [[ ! -d $2 ]]; then
  echo "second argument, the destination directory, is not a directory."
  exit 1
fi

for img in "$1"/*.jpg; do
  img=$(basename "$img")
  ext="${img##*.}"
  base="${img%.*}"
  new_name="$base"_320."$ext"
  echo "$img => $new_name"
  convert "$1"/"$img" -resize 320x320 "$2"/"$new_name"
done
