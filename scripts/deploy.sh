#!/usr/bin/env bash

TARGET_HOSTS=$(avahi-browse -l -p -t -r -a --domain=tractor | grep '^=' | cut --delimiter=";" -f8 | uniq)
